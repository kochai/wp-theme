const settings = {
  "name": "wp-theme",
  "state": {
    "frontity": {
      "url": "https://test.frontity.io",
      "title": "Tišler",
      "description": "Izrada nameštaja\n i fasadne stolarije"
    }
  },
  "packages": [
    {
      "name": "@frontity/tisler",
      "state": {
        "theme": {
          "menu": [
            [
              "Početna",
              "/"
            ],
            [
              "Proizvodi",
              "/#proizvodi",
              "proizvodi"
            ],
            [
              "O nama",
              "/#o-nama",
              "o-nama"
            ],
            [
              "Kontakt",
              "/kontakt/",
              "kontakt"
            ]
          ],
          "featured": {
            "showOnList": true,
            "showOnPost": true,
          }
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "api": "http://test.tisler.com/wp-json",
          "postTypes": [
            {
              "type": "glavna",
              "endpoint": "glavna",
              "archive": "/"
            }
          ],
        },
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "frontity-contact-form-7"
  ]
};

export default settings;
