import React from "react";
import {connect, styled} from "frontity";
import vasTim from "../assets/images/vas-tim.png";
import line from "../assets/images/line.png";

const HomeProduct = ({item, item: {gallery, slug, title: {rendered: title}}}) => {
    console.log(item);
    return (
            <Container className={slug}>
                <Title>
                    {title}
                </Title>
                {
                    gallery.map(({guid}, idx) => (<img src={guid} key={idx}/>))
                }
            </Container>
    );
};

// Connect the HomeProduct component to get access to the `state` in it's `props`
export default connect(HomeProduct);

const Container = styled.section`
    display: grid;
    justify-content: center;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-areas: "about-title about-content about-content";
    grid-area: about;
    background-color: #ff9b6d;
`;

const TitleContainer = styled.div`
    grid-area: about-title;
    width: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
    padding: 20px;
    box-sizing: border-box;
    position: relative;
`;

const Title = styled.h2`
    margin: 0;
    color: #fefefe;
    white-space: pre-line;
    font-family: 'Roboto Light', sans-serif;
    text-transform: uppercase;
    font-size: 4em;
    margin-top: 20px;
    line-height: 1em;
`;

const RightContainer = styled.div`
    width: 100%;
    box-sizing: border-box;
    padding: 70px 40px 150px;
    grid-area: about-content;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-color: #e9eae2;
    background-image: url(${vasTim});
    background-repeat: no-repeat;
    background-position: 80% 95%;
`;

const RightTitle = styled.h2`
    margin: 0;
    color: #333;
    white-space: pre-line;
    font-family: 'Roboto Condensed', sans-serif;
    font-style: italic;
    font-size: 1.4em;
    line-height: 1em;
`;

const RightParagraph = styled.p`
    margin: 0;
    color: #333;
    white-space: pre-line;
    font-family: 'Roboto Condensed Light', sans-serif;
    font-size: 1.4em;
    margin-top: 20px;
    line-height: 1em;
`;
