import React from "react";
import {connect, styled} from "frontity";
import vasTim from "../assets/images/vas-tim.png";
import line from "../assets/images/line.png";

const About = ({state}) => {
    return (
        <>
            <Container id="o-nama">
                <TitleContainer>
                    <svg style={{
                        position: 'absolute',
                        top: '0',
                        bottom: '0',
                        left: '0',
                        right: '0',
                        width: '100%',
                        height: '100%'
                    }}>
                        <line x1="0" y1="100%" x2="100%" y2="0"
                              style={{stroke: "#fefefe", strokeWidth: "2"}}/>
                    </svg>
                    <Title>O nama</Title>
                </TitleContainer>
                <RightContainer>
                    <RightTitle>Kroz godine razvoja...</RightTitle>
                    <RightParagraph>
                        Više od 25 godina prikupljamo iskustvo, stičemo znanja i veštine u stolarstvu.
                        <br/><br/>
                        Od eksploatacije drveta preko proizvodnje prozora i vrata i na kraju opremanja enterijera,
                        razvili smo ponudu svih vrsta stolarskih proizvoda koji su vašem domu ili poslovnom prostoru
                        potrebni. Iza nas je veliki broj projekata izrade fasadne stolarije na stambenim zgradama,
                        privatnim većim i manjim kućama kao i enterijera mnogih domova i pojedinih poslovnih prostora.
                        Proizvodimo sve elemente enterijera od kuhinja, plakara, trepezarijskih stolova, preko kreveta,
                        komoda i ostalog nameštaja.
                        <br/><br/>
                        Pored opremanja enterijera bavimo se izradom prozora, ulaznih i sobnih vrata od drveta,
                        aluminijuma i najsavremenijih kombinacija drvo-aluminijum i aluminijum-drvo.
                        <br/><br/>
                        Snaga našeg malog tima je u svestranosti, dinamičnosti i mogućnosti da odgovori na najširi
                        spektar zahteva u stolarstvu, bilo tradicionlanog ili tehnološki inovativnog.
                    </RightParagraph>
                </RightContainer>
            </Container>
        </>
    );
};

// Connect the About component to get access to the `state` in it's `props`
export default connect(About);

const Container = styled.section`
    display: grid;
    justify-content: center;
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-areas: "about-title about-content about-content";
    grid-area: about;
    background-color: #ff9b6d;
`;

const TitleContainer = styled.div`
    grid-area: about-title;
    width: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
    padding: 20px;
    box-sizing: border-box;
    position: relative;
`;

const Title = styled.h2`
    margin: 0;
    color: #fefefe;
    white-space: pre-line;
    font-family: 'Roboto Light', sans-serif;
    text-transform: uppercase;
    font-size: 4em;
    margin-top: 20px;
    line-height: 1em;
`;

const RightContainer = styled.div`
    width: 100%;
    box-sizing: border-box;
    padding: 70px 40px 150px;
    grid-area: about-content;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-color: #e9eae2;
    background-image: url(${vasTim});
    background-repeat: no-repeat;
    background-position: 80% 95%;
`;

const RightTitle = styled.h2`
    margin: 0;
    color: #333;
    white-space: pre-line;
    font-family: 'Roboto Condensed', sans-serif;
    font-style: italic;
    font-size: 1.4em;
    line-height: 1em;
`;

const RightParagraph = styled.p`
    margin: 0;
    color: #333;
    white-space: pre-line;
    font-family: 'Roboto Condensed Light', sans-serif;
    font-size: 1.4em;
    margin-top: 20px;
    line-height: 1em;
`;
