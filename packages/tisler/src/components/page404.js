import React from "react";
import { styled } from "frontity";

// The 404 page component
const Page404 = () => (
  <Container>
    <Title>Ups, 404!</Title>
    <Description>
      Stranica koju tražite ne može biti pronađena{" "}
      <span role="img" aria-label="confused face">
        😕
      </span>
    </Description>
  </Container>
);

export default Page404;

const Container = styled.div`
  width: 100%;
  background: #ff9b6d;
  margin: 0;
  padding: 24px;
  text-align: center;
`;

const Title = styled.h1`
  margin: 0;
  margin-top: 24px;
  margin-bottom: 8px;
  color: #fefefe;
  font-size: 4em;
`;

const Description = styled.div`
  line-height: 1.6em;
  color: #fefefe;
  margin: 24px 0;
`;
