import React from "react";
import {connect, styled} from "frontity";
import Link from "./link";
import Nav from "./nav";
import logo from '../assets/images/tisler-logo.png';
import logoBackground from '../assets/images/logo-background.png';

const Header = ({state}) => {
    return (
        <>
            <Container>
                <LogoContainer link="/">
                    <StyledLink>
                        <Image src={logo} alt={state.frontity.title}/>
                        <Title>
                            {state.frontity.description}
                        </Title>
                    </StyledLink>
                </LogoContainer>
                <Nav/>
            </Container>
        </>
    );
};

// Connect the Header component to get access to the `state` in it's `props`
export default connect(Header);

const Container = styled.header`
    width: 100%;
    height: 100%;
    max-width: 100%;
    box-sizing: border-box;
    color: #393d3c;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 32%;
    grid-template-areas:
    "logo-container logo-container logo-container navigation"
`;

const Image = styled.img`
    max-width: 30%;
`;

const Title = styled.h2`
    margin: 0;
    white-space: pre-line;
    font-family: 'Roboto Condensed', sans-serif;
    text-transform: uppercase;
    font-size: 20px;
    margin-top: 20px;
    line-height: 1em;
`;

const LogoContainer = styled.div`
    grid-area: logo-container;
    width: 100%;
    background-image: url(${logoBackground});
    background-repeat: no-repeat;
    background-position: 85% 0%;
    background-size: 60%;
`;

const StyledLink = styled(Link)`
    text-decoration: none;
    padding: 20px;
    width: 100%;
    overflow: hidden;
    display: inline-flex;
    justify-content: center;
    flex-direction: column;
`;
