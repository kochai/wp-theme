import React from "react";
import {connect} from "frontity";

const Link = ({
                  state,
                  actions,
                  action,
                  link,
                  className,
                  children,
                  "aria-current": ariaCurrent
              }) => {
    const onClick = event => {
        // Do nothing if it's an external link
        if (link.startsWith("http")) return;

        event.preventDefault();
        // Set the router to the new url.
        actions.router.set(link);

        // Scroll the page to the top
        if (action) {
            setTimeout(() => {
                const element = document.getElementById(action);
                element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
            }, 250);
        } else {
            window.scrollTo(0, 0);
        }
    };

    return (
        <a
            href={link}
            onClick={onClick}
            className={className}
            aria-current={ariaCurrent}
        >
            {children}
        </a>
    );
};

export default connect(Link);
