import React from "react";
import {connect, styled} from "frontity";
import Link from "./link";
import SimpleMap from "./simpleMap";
import logo from "../assets/images/tisler-logo.png";

const Footer = ({state}) => {
    return (
        <>
            <Container>
                <SimpleMap/>
                <RightContainer>
                    <Image src={logo} alt={state.frontity.title}/>
                    <h1>Test</h1>
                </RightContainer>
            </Container>
        </>
    );
};

// Connect the Footer component to get access to the `state` in it's `props`
export default connect(Footer);

const Container = styled.footer`
    display: grid;
    justify-content: center;
    grid-area: footer;
    grid-template-columns: 1fr 1fr;
    background-color: #e9eae2;
`;

const Image = styled.img`
    max-width: 30%;
`;

const Title = styled.h2`
    margin: 0;
    white-space: pre-line;
    font-family: 'Roboto Condensed', sans-serif;
    text-transform: uppercase;
    font-size: 20px;
    margin-top: 20px;
    line-height: 1em;
`;

const RightContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const StyledLink = styled(Link)`
    text-decoration: none;
    padding: 20px;
    width: 100%;
    overflow: hidden;
    display: inline-flex;
    justify-content: center;
    flex-direction: column;
`;
