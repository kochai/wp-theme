import React from "react";
import {Global, css, connect, styled, Head} from "frontity";
import Header from "./header";
import List from "./list";
import Post from "./post";
import Page404 from "./page404.js";
import Loading from "./loading";
import Title from "./title";

import oklahomaRegular from '../assets/fonts/Oklahoma Regular.ttf';
import robotoBlack from '../assets/fonts/Roboto-Black.ttf';
import robotoBlackItalic from '../assets/fonts/Roboto-BlackItalic.ttf';
import robotoBold from '../assets/fonts/Roboto-Bold.ttf';
import robotoBoldItalic from '../assets/fonts/Roboto-BoldItalic.ttf';
import robotoItalic from '../assets/fonts/Roboto-Italic.ttf';
import robotoLight from '../assets/fonts/Roboto-Light.ttf';
import robotoLightItalic from '../assets/fonts/Roboto-LightItalic.ttf';
import robotoMedium from '../assets/fonts/Roboto-Medium.ttf';
import robotoMediumItalic from '../assets/fonts/Roboto-MediumItalic.ttf';
import robotoRegular from '../assets/fonts/Roboto-Regular.ttf';
import robotoThin from '../assets/fonts/Roboto-Thin.ttf';
import robotoThinItalic from '../assets/fonts/Roboto-ThinItalic.ttf';
import robotoCondensedBold from '../assets/fonts/RobotoCondensed-Bold.ttf';
import robotoCondensedBoldItalic from '../assets/fonts/RobotoCondensed-BoldItalic.ttf';
import robotoCondensedItalic from '../assets/fonts/RobotoCondensed-Italic.ttf';
import robotoCondensedLight from '../assets/fonts/RobotoCondensed-Light.ttf';
import robotoCondensedLightItalic from '../assets/fonts/RobotoCondensed-LightItalic.ttf';
import robotoCondensedRegular from '../assets/fonts/RobotoCondensed-Regular.ttf';
import Footer from "./footer";
import About from "./about";

/**
 * Theme is the root React component of our theme. The one we will export
 * in roots.
 */
const Theme = ({state}) => {
    // Get information about the current URL.
    const data = state.source.get(state.router.link);
    const {type} = data;
    const isHomePage = type === 'glavna';

    return (
        <>
            {/* Add some metatags to the <head> of the HTML. */}
            <Title/>
            <Head>
                <meta name="description" content={state.frontity.description}/>
                <html lang="en"/>
            </Head>

            {/* Add some global styles for the whole site, like body or a's.
      Not classes here because we use CSS-in-JS. Only global HTML tags. */}
            <Global styles={globalStyles}/>

            <MainContainer isHome={isHomePage}>
                {/* Add the header of the site. */}
                <HeadContainer>
                    <Header/>
                </HeadContainer>

                {/* Add the main section. It renders a different component depending
      on the type of URL we are in. */}
                <Main>
                    {(data.isFetching && <Loading/>) ||
                    (data.isArchive && <List/>) ||
                    (data.isPostType && <Post/>) ||
                    (data.is404 && <Page404/>)}
                </Main>
                {isHomePage && <About/>}
                <Footer/>
            </MainContainer>
        </>
    );
};

export default connect(Theme);

const globalStyles = css`
    html {
        scroll-behavior: smooth;
    }
    
    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
          "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    
    @font-face {
        font-family: 'Oklahoma Regular';
        src: url(${oklahomaRegular}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }
    
    @font-face {
        font-family: 'Roboto Black';
        src: url(${robotoBlack}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto Light';
        src: url(${robotoLight}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto Medium';
        src: url(${robotoMedium}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto Thin';
        src: url(${robotoThin}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }
    
    @font-face {
        font-family: 'Roboto';
        src: url(${robotoRegular}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto Condensed';
        src: url(${robotoCondensedRegular}) format('ttf');
        font-weight: 400;
        font-style: normal;
    }

    a,
    a:visited {
        color: inherit;
        text-decoration: none;
    }
`;

const HeadContainer = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    background-color: #e9eae2;
    grid-area: header;
    height: 700px;
`;

const MainContainer = styled.div`
    display: grid;
    min-width: 100%;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr;
    grid-template-areas:
    "header"
    "content"
    "about"
    "footer";
`;

const Main = styled.div`
    display: flex;
    justify-content: center;
    grid-area: content;
`;
