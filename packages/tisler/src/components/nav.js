import React, {useState} from "react";
import {connect, styled} from "frontity";
import Link from "./link";
import NavLogo from "../assets/images/icon.png";
import line from "../assets/images/line.png";
import useComponentVisible from "./hooks";

/**
 * Navigation Component
 *
 * It renders the navigation links
 */
const Nav = ({state}) => {
    const {ref, isComponentVisible, setIsComponentVisible} = useComponentVisible(false);

    return (
        <NavContainer>
            <svg style={{
                position: 'absolute',
                top: '0',
                bottom: '0',
                left: '0',
                right: '0',
                width: '100%',
                height: '100%'
            }}>
                <line x1="0" y1="100%" x2="100%" y2="0"
                      style={{stroke: "#e9eae2", strokeWidth: "2"}}/>
            </svg>
            <GridContainer>
                <NavIcon ref={ref} onClick={() => setIsComponentVisible(!isComponentVisible)} row={1} col={3}>
                    <img src={NavLogo}/>
                    <NavMenu isActive={isComponentVisible}>
                        {
                            state.theme.menu.map(([name, link, action]) => {
                                // Check if the link matched the current page url
                                const isCurrentPage = state.router.link === link;
                                return (
                                    <NavItem key={name}>
                                        {/* If link url is the current page, add `aria-current` for a11y */}
                                        <Link link={link} action={action}
                                              aria-current={isCurrentPage ? "page" : undefined}>
                                            {name}
                                        </Link>
                                    </NavItem>
                                );
                            })
                        }
                    </NavMenu>
                </NavIcon>


                <Info row={2} col={1}>
                    Beograd, Srbija <br/>
                    Blok sutjeska <br/>
                    250 U <br/>
                    11210 Kotez <br/>
                </Info>

                <Info row={3} col={2}>
                    +381 63 123 456 <br/>
                    info@tisler.co.rs
                </Info>

            </GridContainer>

        </NavContainer>
    )
};

export default connect(Nav);

const Info = styled.p`
    font-family: 'Roboto Condensed', sans-serif;
    color: #fff;
    text-transform: uppercase;
    padding: 10px;
    line-height: 1.2em;
    font-size: 19px;
    ${props => props.row && props.col ? `grid-column: ${props.col}; grid-row: ${props.row};` : ''}
`;

const NavContainer = styled.div`
    background: #74c7d5;
    position: relative;
`;

const GridContainer = styled.nav`
    list-style: none;
    display: grid;
    grid-template-columns: repeat(3,1fr);
    grid-template-rows: repeat(2,1fr);
    box-sizing: border-box;
    margin: 0;
    overflow-x: visible;
    height: 100%;
    padding: 24px;
`;

const NavMenu = styled.div`
    opacity: 0;
    visibility: hidden;
    display: flex;
    background-color: #f8c255;
    transition: all .25s ease-in-out;
    position: absolute;
    z-index: 100;
    ${props => props.isActive ? 'opacity: 1; visibility: visible;' : ''}
    top: -3px;
    bottom: -3px;
    right: -3px;
    padding-right: 74px;
    padding-left: 45px;
    justify-content: center;
    align-items: center;
`;

const NavIcon = styled.div`
    display: flex;
    position: relative;
    right: 100px;
    justify-content: center;
    align-items: center;
    border: 3px solid #e9eae2;
    padding: 10px;
    height: 50px;
    width: 50px;
    ${props => props.isActive ? 'display: none;' : ''}
    ${props => props.row && props.col ? `grid-column: ${props.col}; grid-row: ${props.row};` : ''}
`;

const NavItem = styled.div`
    padding: 0;
    margin: 0 16px;
    color: #fff;
    font-size: 0.7em;
    box-sizing: border-box;
    flex-shrink: 0;
    
    & > a {
        display: inline-block;
        line-height: 2em;
        font-family: 'Roboto', sans-serif;
        text-transform: uppercase;
        border-bottom: 1px solid;
        border-bottom-color: transparent;
        /* Use for semantic approach to style the current link */
        
        &[aria-current="page"] {
          border-bottom-color: #fff;
        }
    }
    
    &:first-of-type {
        margin-left: 0;
    }
    
    &:last-of-type {
        margin-right: 0;
        
        &:after {
            content: "";
            display: inline-block;
            width: 24px;
        }
    }
`;
