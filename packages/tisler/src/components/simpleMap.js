import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import pin from '../assets/images/pin.png';

const AnyReactComponent = ({text}) => <img src={pin}/>;

class SimpleMap extends Component {
    static defaultProps = {
        center: {
            lat: 44.8521477,
            lng: 20.4629236
        },
        zoom: 16
    };

    createMapOptions() {
        return {
            panControl: false,
            mapTypeControl: false,
            scrollwheel: false,
            zoomControl: false,
            styles: [{stylers: [{'saturation': -100}, {'gamma': 0.8}, {'lightness': 4}, {'visibility': 'on'}]}]
        }
    }

    render() {
        return (
            // Important! Always set the container height explicitly
            <div style={{height: '100vh', width: '100%'}}>
                <GoogleMapReact
                    bootstrapURLKeys={{key: 'AIzaSyAwWKsHt1_GMEmInqUxTP3OkXRajpuwutk'}}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    options={this.createMapOptions}
                >
                    <AnyReactComponent
                        lat={this.props.center.lat}
                        lng={this.props.center.lng}
                        text="My Marker"
                    />
                </GoogleMapReact>
            </div>
        );
    }
}

export default SimpleMap;